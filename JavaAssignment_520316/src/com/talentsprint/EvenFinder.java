package com.talentsprint;

public class EvenFinder {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
		int number = Integer.parseInt(args[0]);
		if (number % 2 == 0)
		{
			System.out.println("true");
		}
		else 
		{
			System.out.println("false");
		}
		}
		catch(Exception e)
		{
			System.out.println("Error");
		}
	}

}
