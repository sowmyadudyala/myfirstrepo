package com.talentsprint;

public class OddRounder {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
		int number = Integer.parseInt(args[0]);
		if(number < 0)
		{
			System.out.println("Error");
		}
		else if(number % 2 == 0)
		{
			System.out.println("Even Number");
		}
		else
		{
			int nextMultiple = ((number / 10) + 1) * 10;
			System.out.println(nextMultiple);
		}
		}
		catch (Exception e)
		{
			System.out.println("Error");
		}
		
	}

}
