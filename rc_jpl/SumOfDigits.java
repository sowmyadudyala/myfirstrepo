package com.talentsprint;

public class SumOfDigits {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int num1 = Integer.parseInt(args[0]);
		int result = (num1 % 10) + (num1 / 10);
		System.out.println(result);
	}



}
