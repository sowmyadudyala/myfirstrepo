package com.talentsprint;

public class DigitChecker {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int num = Integer.parseInt(args[0]);
		int result = Math.abs((num % 10) - (num / 10));
		System.out.println(result);
	}

}
