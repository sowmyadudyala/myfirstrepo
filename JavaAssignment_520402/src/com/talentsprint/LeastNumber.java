package com.talentsprint;

public class LeastNumber {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
		int number1 = Integer.parseInt(args[0]);
		int number2 = Integer.parseInt(args[1]);
		if (number1 < number2)
			System.out.println(number1);
		else
			System.out.println(number2);
		}
		catch (Exception e)
		{
			System.out.println("Error");
		}
		
	}


}
