package com.talentsprint;

public class EvenOrOdd {

	
	public static void main(String[] args) {
		try{
		int num1 = Integer.parseInt(args[0]);
		if(num1 % 2 == 0)
		{
			System.out.println("Even");
		}
		else
		{
			System.out.println("Odd");
		}
		}
		catch(Exception e)
		{
			System.out.println("Error");
		}
	}
		

}
