package com.talentsprint;

public class GreatestNumber {

	public static int getMax(int n1, int n2) {

		if (n1 > n2) {
			return n1;
		} else
			return n2;
	}

	public static void main(String[] args) {
		try {
			int number1 = Integer.parseInt(args[0]);
			int number2 = Integer.parseInt(args[1]);
			int Max = getMax(number1, number2);
			System.out.println(Max);
		} catch (Exception e) {
			System.out.println("Error");
		}

	}

}
